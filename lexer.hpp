#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class Lexer
{
private:
    Lexer(){}
static Lexer *instance;
    
    
    // vector<string> Line;

public:
    static Lexer *get_instance();
    static vector<vector<string>> alllines;
    void create_data();
    vector<string> splite_row(string, vector<string>);
    vector<vector<string>> Get_alllines();
};
